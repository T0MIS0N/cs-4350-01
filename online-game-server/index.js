"use strict";
exports.__esModule = true;
var e = require("express");
var app = e();
var port = 3000;
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var players = {};
server.listen(port, function () {
    console.log("Server is listening on port " + port);
});
app.use(e.static(__dirname));
var numPlayers = 0;
io.on('connection', function (socket) {
    var addedPlayer = false;
    socket.on('add player', function (username) {
        if (addedPlayer) {
            return;
        }
        players[socket.id] = {
            x: 0,
            y: 0,
            playerId: socket.id,
            playerName: username
        };
        socket.emit('currentPlayers', players);
        socket.broadcast.emit('newPlayer', players[socket.id]);
        console.log(username, 'joined the game');
        socket.username = username;
        ++numPlayers;
        addedPlayer = true;
        socket.emit('login', {
            numPlayers: numPlayers
        });
        socket.broadcast.emit('player joined', {
            username: socket.username,
            numPlayers: numPlayers
        });
    });
    socket.on('disconnect', function (username) {
        if (addedPlayer) {
            --numPlayers;
            console.log(socket.username, 'left the game...');
            delete players[socket.id];
            io.emit('disconnect', socket.id);
            socket.broadcast.emit('player left', {
                username: socket.username
            });
        }
    });
    socket.on('playerMovement', function (movementData) {
        players[socket.id].x = 200;
        players[socket.id].y = 200;
        console.log('X:', players[socket.id].X, 'Y:', players[socket.id].X);
        socket.broadcast.emit('playerMoved', players[socket.id]);
    });
});
