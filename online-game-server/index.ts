import e = require('express')
import { UserSocket } from './interfaces';

const app = e();
const port = 3000;

const server = require('http').createServer(app);
const io = require('socket.io')(server);

const players: any= {};

server.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
})

app.use(e.static(__dirname));

var numPlayers = 0;

io.on('connection', (socket: UserSocket) => {
    let addedPlayer = false;
    socket.on('add player', (username) => {
        if (addedPlayer) {
            return;
        }
        players[socket.id]={
            x:0,
            y:0,
            playerId:socket.id,
            playerName:username
        };
        socket.emit('currentPlayers', players);
        socket.broadcast.emit('newPlayer', players[socket.id]);
        console.log(username, 'joined the game')

        socket.username = username;
        ++numPlayers;
        addedPlayer = true;

        socket.emit('login', {
            numPlayers: numPlayers
        });

        socket.broadcast.emit('player joined', {
            username: socket.username,
            numPlayers: numPlayers
        });
    });
    socket.on('disconnect', (username) => {
        if (addedPlayer) {
            --numPlayers;
            console.log(socket.username,'left the game...');
            delete players[socket.id];
            io.emit('disconnect', socket.id);

            socket.broadcast.emit('player left', {
                username: socket.username,
            });
        }
    });

    socket.on('playerMovement',(movementData)=>{
        players[socket.id].x = movementData.x;
        players[socket.id].y = movementData.y;
        console.log('X:',players[socket.id].X,'Y:',players[socket.id].X)

        socket.broadcast.emit('playerMoved', players[socket.id]);
    })
});