export interface ISocketMessage{
    numPlayers?: number;
    username?: string;
    message?: string;
    isLog?: boolean;
    color?: string;
    fade?: boolean;
}