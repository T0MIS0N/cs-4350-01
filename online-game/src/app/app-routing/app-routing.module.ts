import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnologiesUsedComponent } from '../technologies-used/technologies-used.component';

const routes: Routes = [
  {path:'technologies', component: TechnologiesUsedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }