import { SocketEvent } from '../socket-events';
import { ISocketMessage } from '../interfaces';
import * as socket from 'socket.io-client/dist/socket.io';

export class GameScene extends Phaser.Scene {

    public square: Phaser.GameObjects.Rectangle & { body: Phaser.Physics.Arcade.Body };
    private io: { emit: (key: string, value: string) => void, on: (event: string, callback: (data: ISocketMessage) => void) => void };
    private cursorKeys;
    private socket: any;
    public x: number;
    public y: number;

    constructor() {
        super({
            key: "GameScene"
        });
        this.io = socket('http://localhost:3000/');
    }

    preload(): void {

    }

    create(): void {
        //this.socket=this.io();
        this.square = this.add.rectangle(400, 400, 100, 100, 0x1a75ff) as any;
        this.physics.add.existing(this.square);

        this.setUpListeners();

        this.cursorKeys = this.input.keyboard.createCursorKeys();
    }

    update(): void {
        if (this.cursorKeys.up.isDown) {
            this.square.body.setVelocityY(-500);
        } else if (this.cursorKeys.down.isDown) {
            this.square.body.setVelocityY(500);
        } else {
            this.square.body.setVelocityY(0);
        }

        if (this.cursorKeys.left.isDown) {
            this.square.body.setVelocityX(-500);
        } else if (this.cursorKeys.right.isDown) {
            this.square.body.setVelocityX(500);
        } else {
            this.square.body.setVelocityX(0);
        }
        const playerCoordinates = new ObjectCoordinates(this.square.x,this.square.y);
        //This commented out line doesn't work since this socket event needs a string and playerCoordinates is an object.
        //this.io.emit(SocketEvent.PlayerMovement, playerCoordinates);
    }

    createPlayer(playerInfo) {
    }

    addOtherPlayers(playerInfo) {
        const square = this.add.rectangle(400, 400, playerInfo.x, playerInfo.y, 0x1a75ff) as any;
        square.playerID = playerInfo.playerID;
    }

    private setUpListeners() {
        this.io.on('players', (players) => {
            Object.keys(players).forEach((id) => {
                if (players[id].playerID === this.socket.id) {
                } else {
                    this.addOtherPlayers(players[id]);
                }
            });
        });
        this.io.on('newPlayer', (playerInfo) => {
            this.addOtherPlayers(playerInfo)
        });
    }
}
export class ObjectCoordinates{
    protected x:number;
    protected y:number;

    constructor(x:number,y:number){
        this.x = x;
        this.y = y;
    }
}