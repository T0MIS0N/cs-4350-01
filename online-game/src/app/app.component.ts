import { Component, ViewChild, ElementRef, HostBinding, HostListener } from '@angular/core';
import 'phaser';
import { Game, config } from './models/game';
import { ISocketMessage } from './interfaces';
import * as socket from 'socket.io-client/dist/socket.io';
import { SocketMessage } from './socket-messages';
import { SocketEvent } from './socket-events';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title = 'online-game';
  private io: { emit: (key: string, value: string) => void, on: (event: string, callback: (data: ISocketMessage) => void) => void };
  private username: string;
  private isConnected = false;
  public allMessages: ISocketMessage[] = [];
  public messages: string;

  constructor() {
    var game = new Game(config);
    this.io = socket('http://localhost:3000/');
    this.username = 'Square';
    this.setUpListeners();
    this.io.emit(SocketEvent.AddPlayer, this.username);
  }

  public addPlayerMessage(data: ISocketMessage) {
    this.log(data.numPlayers === 1 ? 'There is 1 player' : `There are ${data.numPlayers} players`)
  }

  public log(message: string, shouldPrepend?: boolean) {
    const msg = { isLog: true, message } as ISocketMessage;

    if (shouldPrepend) {
      this.allMessages.unshift(msg)
    } else {
      this.allMessages.push(msg);
    }

    this.scrollToMessageTop();
  }

  public addChatMessage(data: ISocketMessage) {
    data.isLog = false;
    // data.color = this.getUsernameColor(data.username);
    this.allMessages.push(data);
    this.scrollToMessageTop();
  }

  public scrollToMessageTop() {
    if (!this.messages) {
      return;
    }

    //const ul = this.messages.nativeElement;
    //ul.scrollTop = ul.scrollHeight;
  }

  private setUpListeners() {
    this.io.on(SocketEvent.Login, (data) => {
      this.isConnected = true;
      this.log(SocketMessage.Welcome, true);
      this.addPlayerMessage(data);
    });

    this.io.on(SocketEvent.NewMessage, (data) => {
      this.addChatMessage(data);
    });

    this.io.on(SocketEvent.PlayerJoined, (data) => {
      this.log(data.username + ' joined.');
    });
    this.io.on(SocketEvent.PlayerLeft, (data) => {
      this.log(data.username + ' left.');
    });
    this.io.on(SocketEvent.Disconnect, () => {
      this.log(SocketMessage.Disconnected);
    });
    this.io.on(SocketEvent.Reconnect, () => {
      this.log(SocketMessage.Reconnected);
      if (this.username) {
        this.io.emit(SocketEvent.AddPlayer, this.username);
      }
    });
    this.io.on(SocketEvent.ReconnectError, () => {
      this.log(SocketMessage.UnableToReconnect);
    });
  }
}
