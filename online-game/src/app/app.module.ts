import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule} from './app-routing/app-routing.module';
import { TechnologiesUsedComponent } from './technologies-used/technologies-used.component';

@NgModule({
  declarations: [
    AppComponent,
    TechnologiesUsedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
