export const SocketEvent = {
    AddPlayer: 'add player',
    NewMessage: 'new message',
    Login: 'login',
    PlayerJoined: 'player joined',
    PlayerLeft: 'player left',
    Disconnect: 'disconnect',
    Reconnect: 'reconnect',
    ReconnectError: 'reconnect_error',
    PlayerMovement: 'playerMoved'
};