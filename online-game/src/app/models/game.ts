import "phaser/dist/phaser";
import { MainScene } from "../Scenes/scene";
import {GameScene} from "../Scenes/gameScene";

// main game configuration
export const config: Phaser.Types.Core.GameConfig = {
  width: 1000,
  height: 600,
  type: Phaser.AUTO,
  parent: "game",
  scene: GameScene,
  backgroundColor: '#c2c2d6',
  physics: {
    default: "arcade",
    arcade: {

    }
  }
};

// game class
export class Game extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }
}