export const SocketMessage = {
    Welcome: 'Welcome to Untitled Online Game',
    Disconnected: 'You have been disconnected.',
    Reconnected: 'You have been reconnected.',
    UnableToReconnect: 'Attempt to reconnect has failed.'
};